package ru.yandex.money.sergeev;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;

public class ProtectedCode<K, T> {

    static <K, T> ProtectedCode<K, T> protectedCallable(Callable<T> callable) {
        return new ProtectedCode<>(key -> {
            try {
                return callable.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    static <K, Void> ProtectedCode<K, Void> protectedConsumer(Consumer<K> consumer) {
        return new ProtectedCode<>(key -> {
            consumer.accept(key);
            return null;
        });
    }

    static <K, T> ProtectedCode<K, T> protectedFunction(Function<K, T> function) {
        return new ProtectedCode<>(function);
    }

    private final Function<K, T> function;

    private ProtectedCode(Function<K, T> function) {
        this.function = function;
    }

    T executeWithLock(K id) throws RuntimeException {
        return function.apply(id);
    }
}
