package ru.yandex.money.sergeev;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 *  EntityLocker
 *  ------------
 *
 *  The task is to create a reusable utility class that provides
 *  synchronization mechanism similar to row-level DB locking.
 *
 *  The class is supposed to be used by the components that are responsible
 *  for managing storage and caching of different type of entities in the application.
 *  EntityLocker itself does not deal with the entities, only with the IDs (primary keys) of the entities.
 *
 *  Requirements:
 *
 *  1. EntityLocker should support different types of entity IDs.
 *
 *  2. EntityLocker’s interface should allow the caller to specify which entity
*      does it want to work with (using entity ID), and designate the boundaries of the code
*      that should have exclusive access to the entity (called “protected code”).
 *
 *  3. For any given entity, EntityLocker should guarantee that at most one thread executes
 *     protected code on that entity. If there’s a concurrent request to lock the same entity,
 *     the other thread should wait until the entity becomes available.
 *
 *  4. EntityLocker should allow concurrent execution of protected code on different entities.
 *
 *
 *
 *  Bonus requirements (optional):
 *
 *  I. Allow reentrant locking.
 *
 *  II. Allow the caller to specify timeout for locking an entity.
 *
 *  III. Implement protection from deadlocks (but not taking into account possible locks outside EntityLocker).
 *
 *  @param <K> Entity id Type
 */
public class EntityLocker<K> {

    private final ConcurrentHashMap<K, RemovableLock> locks = new ConcurrentHashMap<>();

    /**
     * Выполнение кода с блокировкой по id.
     * @param id Идентификатор ресурса для синхронизации.
     * @param protectedCode Код, выполняемый под монитором.
     * @param <T> Тип возвращаемого значения
     * @return Значение, возвращаемое из синхронизованного кода.
     */
    public <T> T execute(K id, ProtectedCode<K, T> protectedCode) {
        RemovableLock lock = getLock(id);
        try {
            lock.lock();

            return protectedCode.executeWithLock(id);

        } finally {
            unlock(id, lock);
        }
    }

    /**
     * Выполнение кода с блокировкой по id.
     * Попытка блокировки выполняется ограничено по времени.
     *
     * @param id Идентификатор ресурса для синхронизации.
     * @param timeout Максимальное время ожидания захвата лока.
     * @param protectedCode Код, выполняемый под монитором.
     * @param <T> Тип возвращаемого значения
     * @return Значение, возвращаемое из синхронизованного кода, либо {@link Optional#empty()},
     *         если блокировка не была захвачена за указанный период
     */
    public <T> Optional<T> executeWithTimeout(K id, Duration timeout,
                                              ProtectedCode<K, T> protectedCode) throws InterruptedException {
        RemovableLock lock = getLock(id);
        if (!lock.tryLock(timeout.toMillis(), TimeUnit.MILLISECONDS)) {
            lock.decrementUsageCount();
            return Optional.empty();
        }
        try {

            return Optional.of(protectedCode.executeWithLock(id));

        } finally {
            unlock(id, lock);
        }
    }

    private RemovableLock getLock(K id) {
        return locks.compute(id, (k, existedLock) -> {
            if (existedLock == null
                    || existedLock.isRemoving() && !existedLock.isCurrentlyUsed()) {
                RemovableLock result = new RemovableLock();
                result.incrementUsageCount();
                return result;
            }
            existedLock.incrementUsageCount();
            return existedLock;
        });
    }

    private void unlock(K id, RemovableLock lock) {
        if (!lock.isCurrentlyUsed()) {
            RemovableLock removingCopy = lock.removingCopy();
            lock.setRemoving(true);
            if (lock.isCurrentlyUsed()) {
                // Кто-то снова захватил лок после начала удаления. Отменяем попытку.
                lock.setRemoving(false);
            } else {
                // Объект не удалится, если состояние лока поменялось.
                locks.remove(id, removingCopy);
            }
        }
        lock.unlock();
    }

    /**
     * Итерация по всей мапе с удалением неиспользуемых локов.
     */
    void removeStaleLocks() {
        locks.entrySet().removeIf(entry -> {
            RemovableLock lock = entry.getValue();
            lock.setRemoving(true);
            if (lock.getUsageCount() != 0) {
                lock.setRemoving(false);
                return false;
            }
            return lock.isRemoving();
        });
    }

    /**
     * Метод для тестов, позволяющий получить текущее число используемых локов.
     *
     * @return Количество активных локов
     */
    int getLocksCount() {
        return locks.size();
    }
}