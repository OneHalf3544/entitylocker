package ru.yandex.money.sergeev;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Удаляемый лок.
 * Содержит в себе признак "удаляемости" для двухфазного выключения объекта из использования.
 */
class RemovableLock {

    private final ReentrantLock lock;
    private final AtomicInteger usageCount;
    private volatile boolean removing;

    RemovableLock() {
        this(new ReentrantLock(), false, 0);
    }

    /**
     * Искользуется ли лок в данный момент.
     */
    boolean isCurrentlyUsed() {
        return usageCount.get() > 1;
    }

    private RemovableLock(ReentrantLock lock, boolean removing, int count) {
        this.lock = lock;
        this.setRemoving(removing);
        usageCount = new AtomicInteger(count);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RemovableLock that = (RemovableLock) o;
        return isRemoving() == that.isRemoving()
                && Objects.equals(lock, that.lock)
                && usageCount.get() == that.usageCount.get();
    }

    @Override
    public int hashCode() {
        return Objects.hash(lock, isRemoving(), usageCount.get());
    }

    void lock() {
        lock.lock();
    }

    /**
     * Захват блокировки с таймаутом.
     *
     * @param timeout Таймаут
     * @param timeUnit Единицы измерения таймаута.
     * @return true, если удалось захватить блокировку.
     * @throws InterruptedException При остановке потока во время ожидания.
     */
    boolean tryLock(long timeout, TimeUnit timeUnit) throws InterruptedException {
        return lock.tryLock(timeout, timeUnit);
    }

    /**
     *
     * @return true, если выполняется удаление лока.
     */
    boolean isRemoving() {
        return removing;
    }

    /**
     * Выставление признака удаления.
     *
     * @param removing новое значение флага
     */
    void setRemoving(boolean removing) {
        this.removing = removing;
    }

    /**
     * Снятие блокировки.
     */
    void unlock() {
        lock.unlock();
        usageCount.decrementAndGet();
    }

    /**
     * Создание копии объекта с признаком "удаляемости"
     * @return копия объекта, с измененным флагом.
     */
    RemovableLock removingCopy() {
        return new RemovableLock(lock, true, 1);
    }

    void incrementUsageCount() {
        usageCount.incrementAndGet();
    }

    void decrementUsageCount() {
        usageCount.decrementAndGet();
    }

    int getUsageCount() {
        return usageCount.get();
    }

    @Override
    public String toString() {
        return "RemovableLock{" +
                "lock=" + lock +
                ", usageCount=" + usageCount +
                ", removing=" + removing +
                '}';
    }
}
