package ru.yandex.money.sergeev;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.ReentrantLock;

import static org.junit.jupiter.api.Assertions.*;

class RemovableLockTest {
    @Test
    void should_implementEqualsContract() {
        EqualsVerifier.forClass(RemovableLock.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .withPrefabValues(ReentrantLock.class, new ReentrantLock(), new ReentrantLock())
                .withNonnullFields("usageCount")
                .verify();
    }

}