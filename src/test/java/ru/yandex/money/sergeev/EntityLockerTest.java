package ru.yandex.money.sergeev;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Optional.empty;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;
import static ru.yandex.money.sergeev.ProtectedCode.*;

class EntityLockerTest {

    private static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(4);

    @Test
    void should_executeInParallel_when_idsIsDifferent() throws BrokenBarrierException, InterruptedException {
        EntityLocker<Long> entityLocker = new EntityLocker<>();
        AtomicBoolean barrierWasActivated = new AtomicBoolean(false);

        CyclicBarrier barrier = new CyclicBarrier(2, () -> barrierWasActivated.set(true));

        List<Future<Integer>> futures = THREAD_POOL.invokeAll(List.<Callable<Integer>>of(
                () -> entityLocker.execute(123L, protectedCallable(barrier::await)),
                () -> entityLocker.execute(124L, protectedCallable(barrier::await))),
                1, SECONDS
        );

        assertTrue(futures.stream().allMatch(Future::isDone));
        assertTrue(futures.stream().noneMatch(Future::isCancelled));
        assertTrue(barrierWasActivated.get());
        assertThat(entityLocker.getLocksCount(), is(0));
    }

    @Test
    void should_notGetDeadlock_when_executeTwiceForOneThread() throws BrokenBarrierException, InterruptedException {
        EntityLocker<Long> entityLocker = new EntityLocker<>();

        entityLocker.execute(123L, protectedCallable(
                () -> entityLocker.execute(123L, protectedCallable(() -> true))));

        assertThat(entityLocker.getLocksCount(), is(0));
    }

    @Test
    void should_waitUnlock_when_usedForTheSameId() throws InterruptedException, ExecutionException {
        EntityLocker<Long> entityLocker = new EntityLocker<>();

        AtomicInteger count = new AtomicInteger(0);
        CountDownLatch firstTaskCanContinue = new CountDownLatch(1);
        CountDownLatch firstTaskRun = new CountDownLatch(1);

        Future<Object> first = THREAD_POOL.submit(
                () -> entityLocker.execute(123L, protectedCallable(() -> {
                    firstTaskRun.countDown();
                    firstTaskCanContinue.await();
                    count.incrementAndGet();
                    return count.get();
                }))
        );


        // Run second thread after first:
        firstTaskRun.await();
        assertThat(entityLocker.getLocksCount(), is(1));
        entityLocker.removeStaleLocks();
        assertThat(entityLocker.getLocksCount(), is(1));

        Future<Integer> second = THREAD_POOL.submit(
                () -> entityLocker.execute(123L, protectedConsumer((id) -> count.incrementAndGet())));

        // waiting for countDownLatch:
        assertThrows(TimeoutException.class, () -> first.get(100, MILLISECONDS));
        // waiting for unlock by the first thread:
        assertThrows(TimeoutException.class, () -> second.get(100, MILLISECONDS));

        assertEquals(0, count.intValue());
        assertThat(entityLocker.getLocksCount(), is(1));

        firstTaskCanContinue.countDown();
        first.get();
        second.get();

        assertEquals(2, count.intValue());
        assertThat(entityLocker.getLocksCount(), is(0));
    }

    @Test
    void should_unlock_when_exceptionThrown() throws ExecutionException, InterruptedException {
        EntityLocker<Long> entityLocker = new EntityLocker<>();

        Future<Object> first = THREAD_POOL.submit(
                () -> entityLocker.execute(123L, protectedCallable(() -> {
                    throw new Exception("exception");
                }))
        );

        assertThrows(ExecutionException.class, first::get);
        assertThat(entityLocker.getLocksCount(), is(0));
    }

    @Test
    void should_returnOptionalEmpty_when_lockedIdRequestedWithTimeout() throws InterruptedException, ExecutionException {
        EntityLocker<Long> entityLocker = new EntityLocker<>();

        CountDownLatch taskCanContinue = new CountDownLatch(1);
        CountDownLatch firstTaskRun = new CountDownLatch(1);

        Future<String> first = THREAD_POOL.submit(
                () -> entityLocker.execute(123L, protectedCallable(() -> {
                    firstTaskRun.countDown();
                    taskCanContinue.await();
                    return "result";
                })));

        // Run second thread after first:
        firstTaskRun.await();

        Optional<String> optional = entityLocker.executeWithTimeout(
                123L, Duration.ofSeconds(1), protectedFunction((id) -> "weCannotGetThisResult"));

        assertThat(optional, is(empty()));
        assertThat(entityLocker.getLocksCount(), is(1));

        taskCanContinue.countDown();

        first.get();

        optional = entityLocker.executeWithTimeout(
                123L, Duration.ofSeconds(1), protectedFunction((id) -> "result"));

        assertThat(optional, is(Optional.of("result")));
        assertThat(entityLocker.getLocksCount(), is(0));
    }


}